package com.example.notes.notesapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback
import android.widget.Button
import android.widget.EditText
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick

/**
 * Created by Elina on 12/1/16.
 */

class MainActivity() : AppCompatActivity() {

    lateinit var adapter: ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layout = mainLayout()

        setContentView(layout)

        supportActionBar?.title = "Заметки"

        val recyclerView = layout.find<RecyclerView>(android.R.id.list)
        adapter = ListAdapter(this)
        recyclerView.adapter = adapter

        if (savedInstanceState != null) {
            adapter.restoreNotes(savedInstanceState)
        }

        val inputView = layout.find<EditText>(android.R.id.edit)

        layout.find<Button>(android.R.id.button1).onClick {
            if (inputView.text.isNotEmpty()) {
                adapter.addNote(inputView.text.toString())
                inputView.setText("")
            }
        }

        val simpleItemTouchCallback: SimpleCallback =
                object : SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                    override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean = true

                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                        adapter.removeNote(viewHolder.layoutPosition)
                    }
                }

        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if (outState != null) {
            adapter.saveNotes(outState)
        }
    }
}