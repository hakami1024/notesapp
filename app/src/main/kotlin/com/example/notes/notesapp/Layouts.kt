package com.example.notes.notesapp

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * Created by Elina on 12/1/16.
 */

fun Context.noteRow(): ViewGroup =
        frameLayout {
            layoutParams = RecyclerView.LayoutParams(matchParent, wrapContent).apply {
                verticalMargin = dip(5)
            }

            backgroundColor = resources.getColor(R.color.rowBackground)

            textView {
                id = android.R.id.text1
            }.lparams {
                width = matchParent
                height = wrapContent
                padding = dip(10)
            }
        }

fun Context.mainLayout(): LinearLayout {
    return verticalLayout {
        layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)

        editText {
            id = android.R.id.edit
            hint = "Введите заметку"
        }.lparams {
            width = matchParent
            height = wrapContent
            padding = dip(10)
        }
        button {
            id = android.R.id.button1
            text = "Создать"
            backgroundColor = resources.getColor(R.color.colorAccent)
            textColor = Color.WHITE
        }.lparams {
            width = wrapContent
            height = wrapContent
            gravity = Gravity.RIGHT
        }
        recyclerView {
            id = android.R.id.list
            topPadding = dip(10)
            layoutManager = LinearLayoutManager(ctx)
        }.lparams {
            width = matchParent
            height = matchParent
        }
    }
}