package com.example.notes.notesapp

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.find

/**
 * Created by Elina on 12/1/16.
 */

class NoteHolder(val view: ViewGroup, val name: TextView) : RecyclerView.ViewHolder(view)

fun NoteHolder.bind(note: String) {
    name.text = note
}

private val SAVED_LIST = "notes_list"

class ListAdapter(val context: Context) : RecyclerView.Adapter<NoteHolder>() {
    private var items = arrayListOf<String>()

    fun saveNotes(bundle: Bundle) {
        bundle.putStringArrayList(SAVED_LIST, items)
    }

    fun restoreNotes(bundle: Bundle) {
        if (bundle.containsKey(SAVED_LIST)) {
            items = bundle.getStringArrayList(SAVED_LIST)
            notifyDataSetChanged()
        }
    }

    fun addNote(note: String) {
        items.add(note)
        notifyDataSetChanged()
    }

    fun removeNote(i: Int) {
        items.removeAt(i)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int =
            items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val view = context.noteRow()
        val holder = NoteHolder(view, view.find(android.R.id.text1))

        return holder
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) =
            holder.bind(items[position])
}
